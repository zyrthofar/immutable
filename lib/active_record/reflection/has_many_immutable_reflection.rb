# frozen_string_literal: true

module ActiveRecord
  module Reflection
    # Reflection implementation.
    class HasManyImmutableReflection < HasManyReflection
      def macro
        :has_many_immutable
      end

      def association_class
        Associations::HasManyImmutableAssociation
      end

      private

      # Immutable records should never be constructed programmatically,
      # except in migrations, and using the model itself.
      def calculate_constructable(_macro, _options)
        false
      end
    end
  end
end
