# frozen_string_literal: true

module ActiveRecord
  module Reflection
    # Reflection implementation.
    class BelongsToImmutableReflection < BelongsToReflection
      # Type of reflection.
      def macro
        :belongs_to_immutable
      end

      def association_class
        if polymorphic?
          raise NotImplementedError,
                'Immutable polymorphism not implemented yet.'
        end

        Associations::BelongsToImmutableAssociation
      end

      private

      # Immutable records should never be constructed programmatically,
      # except in migrations, and using the model itself.
      def calculate_constructable(_macro, _options)
        false
      end
    end
  end
end
