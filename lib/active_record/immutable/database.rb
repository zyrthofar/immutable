# frozen_string_literal: true

module ActiveRecord
  module Immutable
    # Provides database-access methods to immutable records.
    module Database
      extend ActiveSupport::Concern

      included do
        class << self
          private

          # Retrieves code values from the database.
          def retrieve_persisted_code_values(ids)
            code_values = []
            records = where(primary_key => ids)

            ids.each do |id|
              record = records.find { |r| r.id == id }

              code_value = record&.code_value
              code_values << code_value

              update_cached_code_value(id, code_value)
              update_cached_record(code_value, record) if code_value
            end

            code_values.uniq
          end

          # Retrieves records from the database.
          def retrieve_persisted_records(code_values)
            records = where(immutable_code_attribute => code_values)

            code_values.each do |code_value|
              record = records.find { |r| r.code_value == code_value }

              update_cached_code_value(record.id, code_value) if record
              update_cached_record(code_value, record)
            end

            records.to_a
          end
        end
      end
    end
  end
end
