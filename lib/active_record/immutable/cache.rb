# frozen_string_literal: true

module ActiveRecord
  module Immutable
    # Provides cache-access methods to immutable records.
    module Cache
      extend ActiveSupport::Concern

      included do
        class << self
          # Returns true if the code value associated to the specified id can be
          # found in the cache.
          def code_value_cached?(id)
            check_cached_code_value(id)
          end

          # Returns true if the record associated to the specified code value
          # can be found in the cache.
          def record_cached?(code_value)
            check_cached_record(code_value)
          end

          # Deletes all cache keys related to this model. It is highly
          # recommended to clear the cache after making changes to one or more
          # immutable records, and to do so in a migration.
          #
          # ```ruby
          # class CreateEmailContactKind < ActiveRecord::Migration
          #   def change
          #     reversible do |dir|
          #       dir.up { ContactKind.create!(code: 'email') }
          #       dir.down { ContactKind[:email].destroy! }
          #     end
          #
          #     ContactKind.clear_cache!
          #   end
          # end
          # ```
          def clear_cache!
            delete_cache
          end

          protected

          # Default prefix for the cache keys.
          def cache_key_prefix
            "immutable/#{model_name.cache_key}/"
          end

          # Default cache key for code values.
          def cached_code_value_key(id)
            "#{cache_key_prefix}ids/#{id}"
          end

          # Default cache key for records.
          def cached_record_key(code_value)
            "#{cache_key_prefix}code_values/#{code_value}"
          end

          private

          # Verifies whether the code value is in the cache.
          def check_cached_code_value(id)
            key = cached_code_value_key(id)
            Rails.cache.exist?(key)
          end

          # Verifies whether the record is in the cache.
          def check_cached_record(code_value)
            key = cached_record_key(code_value)
            Rails.cache.exist?(key)
          end

          # Retrieves a code value from the cache.
          def retrieve_cached_code_value(id)
            key = cached_code_value_key(id)
            Rails.cache.read(key)
          end

          # Retrieves a record form the cache.
          def retrieve_cached_record(code_value)
            key = cached_record_key(code_value)
            Rails.cache.read(key)
          end

          # Updates a code value in the cache.
          def update_cached_code_value(id, code_value)
            key = cached_code_value_key(id)
            Rails.cache.write(key, code_value, expires_in: 1.week)
          end

          # Updates a record in the cache.
          def update_cached_record(code_value, record)
            key = cached_record_key(code_value)
            Rails.cache.write(key, record, expires_in: 1.week)
          end

          # Deletes all cache related to the immutable model.
          def delete_cache
            Rails.cache.delete_matched(cache_key_prefix)
          end
        end
      end
    end
  end
end
