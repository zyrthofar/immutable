# frozen_string_literal: true

require 'active_record/immutable/cache'
require 'active_record/immutable/database'

module ActiveRecord
  # Module that contains the methods that will be available to immutable models.
  #
  # An immutable model is assumed never to change. This assumption allows the
  # use of a model-wide cache, which prevents unnecessary database queries.
  #
  # This module must be included in a model to make it immutable:
  # ```ruby
  # class UserStatus < ActiveRecord::Base
  #   include ActiveRecord::Immutable
  #
  #   ...
  # end
  # ```
  module Immutable
    extend ActiveSupport::Concern

    include Cache
    include Database

    included do
      # Returns a record's code value, based on the configured code attribute.
      #
      # ```ruby
      # user.default_currency
      # # => <Currency id: 6, code: "USD", name: "United States dollar">
      #
      # user.default_currency.code_value
      # # => "USD"
      # ```
      def code_value
        read_attribute(self.class.immutable_code_attribute)
      end

      class << self
        # Retrieves a record by the specified code value, using the cache if
        # available, or from the database otherwise. If the argument is an
        # instance of the model, it will be returned.
        #
        # The specified code value can be anything that is convertible to a
        # string - symbols are a popular choice to represent codes inside a
        # project.
        #
        # ```ruby
        # Country[:CAN]
        # # => <Country id: 2, code: "CAN", name: "Canada">
        # ```
        def [](code_value)
          return code_value if code_value.is_a?(self)

          retrieve_record(code_value)
        end

        # Retrieves a single code value by the specified id, using the cache if
        # possible, or from the database otherwise.
        def retrieve_code_value(id)
          code_values = retrieve_code_values([id])
          code_values.first
        end

        # Retrieves a list of code values by the specified ids, using the cache
        # if possible, or from the database otherwise.
        def retrieve_code_values(ids)
          ids = ids.dup
          code_values = []

          ids.reject! do |id|
            if code_value_cached?(id)
              code_values << retrieve_cached_code_value(id)
              true
            end
          end

          code_values += retrieve_persisted_code_values(ids) if ids.any?
          code_values.uniq!
          code_values.compact! || code_values
        end

        # Retrieves a single record by the specified code value, using the cache
        # if possible, or from the database otherwise.
        def retrieve_record(code_value)
          records = retrieve_records([code_value])
          records.first
        end

        # Retrieves a list of records by the specified code values, using the
        # cache if possible, or from the database otherwise.
        def retrieve_records(code_values)
          code_values = code_values.dup.map(&:to_s)
          records = []

          code_values.reject! do |code_value|
            if record_cached?(code_value)
              records << retrieve_cached_record(code_value)
              true
            end
          end

          records += retrieve_persisted_records(code_values) if code_values.any?
          records.uniq!
          records.compact! || records
        end

        # Default code attribute. Override to match the model's.
        def immutable_code_attribute
          :code
        end
      end
    end
  end
end
