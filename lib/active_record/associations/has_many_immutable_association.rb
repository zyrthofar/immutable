# frozen_string_literal: true

require 'active_record/associations/builder/has_many_immutable'
require 'active_record/reflection/has_many_immutable_reflection'

module ActiveRecord
  module Associations
    # Class methods on `ActiveRecord::Associations`.
    module ClassMethods
      # Allows a model to declare associations to immutable models. This
      # association is similar to a normal `has_many through` association, with
      # one difference being the immutable record cache that will be used
      # instead of the ActiveRecord cache.
      #
      # Contrary to a normal `has_many through` association, the `has_many`
      # association that declares the joint table is optional. It will be
      # created automatically if missing.
      #
      # ```ruby
      # class User < ActiveRecord::Base
      #   has_many_immutable :user_permissions
      # end
      # ```
      def has_many_immutable(name, options = {})
        unless reflect_on_association(options[:through])
          # Automatically creates the association for the join table.
          has_many options[:through]
        end

        reflection =
          Builder::HasManyImmutable.build(
            self,
            name,
            nil,
            options
          )

        Reflection.add_reflection(
          self,
          name,
          reflection
        )
      end
    end

    # Specific implementation of a `has_many_immutable` association.
    class HasManyImmutableAssociation < HasManyThroughAssociation
      # This retrieves all the code value by the immutable record ids, then
      # queries the cache and/or the database to find the immutable records.
      #
      # Warning: contrary to the normal behavior, this does not return a
      # `CollectionProxy`, but a simple array instead.
      def reader
        code_values = klass.retrieve_code_values(ids_reader)
        klass.retrieve_records(code_values)
      end
    end
  end
end
