# frozen_string_literal: true

require 'active_record/associations/builder/belongs_to_immutable'
require 'active_record/reflection/belongs_to_immutable_reflection'

module ActiveRecord
  module Associations
    # Class methods on `ActiveRecord::Associations`.
    module ClassMethods
      # Allows a model to declare an association to an immutable model. This
      # association is very similar to a normal `belongs_to` association, with
      # the main difference being the immutable record cache that will be
      # used instead of the ActiveRecord cache.
      #
      # ```ruby
      # class User < ActiveRecord::Base
      #   belongs_to_immutable :user_status
      # end
      # ```
      def belongs_to_immutable(name, options = {})
        reflection =
          Builder::BelongsToImmutable.build(
            self,
            name,
            nil,
            options
          )

        Reflection.add_reflection(
          self,
          name,
          reflection
        )
      end
    end

    # Specific implementation of a `belongs_to_immutable` association.
    class BelongsToImmutableAssociation < BelongsToAssociation
      # This retrieves the code value by the immutable record id, then queries
      # the cache or the database to find the immutable record.
      def reader
        code_value = klass.retrieve_code_value(target_id)
        klass.retrieve_record(code_value)
      end
    end
  end
end
