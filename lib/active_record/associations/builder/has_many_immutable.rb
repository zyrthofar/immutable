# frozen_string_literal: true

module ActiveRecord
  module Associations
    module Builder
      # Defines how to build a has_many_immutable reflection.
      class HasManyImmutable < HasMany
        # Type of association builder.
        def macro
          :has_many_immutable
        end

        # No dependent options available.
        def self.valid_dependent_options
          []
        end

        # This completely overrides `Association`'s `create_reflection` method.
        # I don't see any way around this, with how `Reflection.create` is
        # coded.
        def self.create_reflection(model, name, scope, options, extension = nil)
          unless name.is_a?(Symbol)
            raise ArgumentError, 'association names must be a Symbol'
          end

          if scope.is_a?(Hash)
            options = scope
            scope   = nil
          end

          validate_options(options)

          scope = build_scope(scope, extension)
          build_reflection(model, name, scope, options)
        end

        def self.build_reflection(model, name, scope, options)
          reflection =
            ActiveRecord::Reflection::HasManyImmutableReflection.new(
              name,
              scope,
              options,
              model
            )

          ActiveRecord::Reflection::ThroughReflection.new(reflection)
        end
      end
    end
  end
end
