# frozen_string_literal: true

module ActiveRecord
  module Associations
    module Builder
      # Defines how to build a belongs_to_immutable reflection.
      class BelongsToImmutable < BelongsTo
        # Type of association builder.
        def macro
          :belongs_to_immutable
        end

        # No dependent options available.
        def self.valid_dependent_options
          []
        end

        # This completely overrides `Association`'s `create_reflection` method.
        # I don't see any way around this, with how `Reflection.create` is
        # coded.
        def self.create_reflection(model, name, scope, options, extension = nil)
          unless name.is_a?(Symbol)
            raise ArgumentError, 'association names must be a Symbol'
          end

          if scope.is_a?(Hash)
            options = scope
            scope   = nil
          end

          validate_options(options)

          scope = build_scope(scope, extension)
          build_reflection(model, name, scope, options)
        end

        # Build the association's reflection.
        def self.build_reflection(model, name, scope, options)
          reflection =
            ActiveRecord::Reflection::BelongsToImmutableReflection.new(
              name,
              scope,
              options,
              model
            )

          # TODO: Test through association.
          options[:through] ? ThroughReflection.new(reflection) : reflection
        end
      end
    end
  end
end
