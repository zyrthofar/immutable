# frozen_string_literal: true

require 'active_record'
require 'active_record/associations/belongs_to_immutable_association'
require 'active_record/associations/has_many_immutable_association'
require 'active_record/immutable'
