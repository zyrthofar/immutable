# README

The `Immutable` gem can be used to simplify and optimize handling of static active records. Such records may be a database list of countries, user kinds, or notification statuses.

Important note: These immutable records must be *uniquely* described by a single code attribute. Furthermore, since immutable records rely blindly on a model-based cache, this gem should *not* be used on records that could change during a normal flow. Whenever an immutable record needs to be created, modified, or destroyed, it is highly recommended to do this inside a migration and manually clear the record's cache.

[![pipeline status](https://gitlab.com/zyrthofar/immutable/badges/master/pipeline.svg)](https://gitlab.com/zyrthofar/immutable/commits/master)
[![coverage report](https://gitlab.com/zyrthofar/immutable/badges/master/coverage.svg)](https://gitlab.com/zyrthofar/immutable/commits/master)

## Usage

The gem can be included in the project with the addition of the following line in the `Gemfile` file:
```ruby
gem 'immutable', git: 'https://gitlab.com/zyrthofar/immutable.git'
```

The `ActiveRecord::Immutable` module must be included in the model definition to define that model as immutable:
```ruby
class Status < ActiveRecord::Base
  include ActiveRecord::Immutable

  ...
end
```

This enables a model-based cache, which will be used throughout the project.

Using the `.[]` method with a code value will look in the model cache, then in the database if necessary.
```ruby
Country[:CAN]
# => #<Country id: 2, code: "CAN", name: "Canada">
```

## Associations

Immutable records can also benefit when they are associated to parent records.

One-to-many and many-to-many associations are declared using `belongs_to_immutable` and `has_many_immutable`, respectively.
```ruby
class User < ActiveRecord::Base
  belongs_to_immutable :status
  has_many_immutable :permissions, through: :user_permissions
end
```

See the [repository's associations wiki page](https://gitlab.com/zyrthofar/immutable/wikis/associations) for usage and information.

## Wiki page

The [repository's wiki](https://gitlab.com/zyrthofar/immutable/wikis/home) can be referred to for more details and information than this summary.
