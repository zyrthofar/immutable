# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'immutable'
  s.version = '0.2.2'
  s.date = '2018-03-03'
  s.summary = 'Immutability for active records.'
  s.description = 'Immutability for active records.'
  s.authors = ['Zyrthofar']
  s.email = 'zyrthofar@gmail.com'
  s.homepage = 'https://gitlab.com/zyrthofar/immutable'
  s.license = 'MIT'

  s.files = %w[
    lib/immutable.rb
    lib/active_record/associations.rb
    lib/active_record/associations/belongs_to_immutable_association.rb
    lib/active_record/associations/has_many_immutable_association.rb
    lib/active_record/associations/builder/belongs_to_immutable.rb
    lib/active_record/associations/builder/has_many_immutable.rb
    lib/active_record/immutable.rb
    lib/active_record/immutable/cache.rb
    lib/active_record/immutable/database.rb
    lib/active_record/reflection/belongs_to_immutable_reflection.rb
    lib/active_record/reflection/has_many_immutable_reflection.rb
  ]
end
