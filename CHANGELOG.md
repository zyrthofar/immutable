# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.2] - 2018-03-03
### Changed
- [!29](https://gitlab.com/zyrthofar/immutable/merge_requests/29) Remove `retrieve_*` alias in favor of a proper method.

### Removed
- [!28](https://gitlab.com/zyrthofar/immutable/merge_requests/28) Comparing immutable records by code values.
- [!28](https://gitlab.com/zyrthofar/immutable/merge_requests/28) Setting associations by code values.

## [0.2.1] - 2018-02-18
### Changed
- [!24](https://gitlab.com/zyrthofar/immutable/merge_requests/24) Optimization of code values and records retrieval from the database.

## [0.2.0] - 2018-02-02
Base version with all features in place.
