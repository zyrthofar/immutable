# frozen_string_literal: true

FactoryBot.define do
  factory :integer_code_immutable_record do
    sequence(:code)
  end
end
