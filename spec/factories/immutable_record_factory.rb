# frozen_string_literal: true

FactoryBot.define do
  factory :immutable_record do
    sequence(:code) { |n| "CODE_#{n}" }
  end
end
