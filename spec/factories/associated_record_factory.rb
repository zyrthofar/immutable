# frozen_string_literal: true

FactoryBot.define do
  factory :associated_record do
    immutable_record
  end
end
