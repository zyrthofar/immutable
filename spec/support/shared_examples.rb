# frozen_string_literal: true

RSpec.shared_examples :code_values_retrieved_from_cache do
  it 'retrieves the code values from the cache' do
    Array(expected_ids).each do |expected_id|
      expect(ImmutableRecord).to(
        receive(:retrieve_cached_code_value)
          .with(expected_id)
      ).and_call_original
    end

    allow(ImmutableRecord).to(
      receive(:retrieve_persisted_code_values)
    ).and_call_original

    expect(ImmutableRecord).not_to(
      receive(:retrieve_persisted_code_values)
        .with(array_including(Array(expected_ids)))
    )

    subject
  end
end

RSpec.shared_examples :code_values_retrieved_from_database do
  it 'retrieves the code values from the database' do
    Array(expected_ids).each do |expected_id|
      expect(ImmutableRecord).not_to(
        receive(:retrieve_cached_code_value)
          .with(expected_id)
      )
    end

    expect(ImmutableRecord).to(
      receive(:retrieve_persisted_code_values)
        .with(Array(expected_ids))
    ).and_call_original

    subject
  end
end

RSpec.shared_examples :records_retrieved_from_cache do
  it 'retrieves the records from the cache' do
    Array(expected_code_values).each do |expected_code_value|
      expect(ImmutableRecord).to(
        receive(:retrieve_cached_record).with(expected_code_value)
      ).and_call_original
    end

    allow(ImmutableRecord).to(
      receive(:retrieve_persisted_records)
    ).and_call_original

    expect(ImmutableRecord).not_to(
      receive(:retrieve_persisted_records)
        .with(array_including(Array(expected_code_values)))
    )

    subject
  end
end

RSpec.shared_examples :records_retrieved_from_database do
  it 'retrieves the records from the database' do
    Array(expected_code_values).each do |expected_code_value|
      expect(ImmutableRecord).not_to(
        receive(:retrieve_cached_record)
          .with(expected_code_value)
      )
    end

    expect(ImmutableRecord).to(
      receive(:retrieve_persisted_records)
        .with(Array(expected_code_values))
    ).and_call_original

    subject
  end
end
