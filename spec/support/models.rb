# frozen_string_literal: true

class ImmutableRecord < ActiveRecord::Base
  include ActiveRecord::Immutable

  attribute :code, :string
end

class IntegerCodeImmutableRecord < ActiveRecord::Base
  include ActiveRecord::Immutable

  attribute :code, :integer
end

class AssociatedRecord < ActiveRecord::Base
  belongs_to_immutable :immutable_record
  belongs_to_immutable :immutable_polymorphic_record, polymorphic: true

  has_many_immutable :immutable_records,
                     through: :associated_record_immutable_records
end

class AssociatedRecordImmutableRecord < ActiveRecord::Base
  belongs_to :associated_record
  belongs_to :immutable_record
end
