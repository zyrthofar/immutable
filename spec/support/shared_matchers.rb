# frozen_string_literal: true

RSpec::Matchers.define :cache_code_values do |expected_code_values|
  match do
    expected_code_values = Array(expected_code_values).map(&:to_s)
    actual_code_values.each_with_index do |actual_code_value, index|
      expect(actual_code_value).to eq(expected_code_values[index])
    end
  end

  define_method :actual_ids do
    Array(defined?(ids) && ids || id)
  end

  define_method :actual_code_values do
    actual_ids.map do |id|
      key = "immutable/immutable_records/ids/#{id}"
      Rails.cache.read(key)
    end
  end

  define_method :failed_ids do
    failures = []

    actual_code_values.each_with_index do |actual_code_value, index|
      id = actual_ids[index]
      expected_code_value = Array(expected_code_values)[index]

      next if actual_code_value == expected_code_value

      failures << {
        id: id,
        actual: actual_code_value,
        expected: expected_code_value
      }
    end

    failures
  end

  description do
    code_values = Array(expected_code_values)

    if code_values.one?
      "cache code value '#{code_values.first}'"
    else
      "cache code values '#{code_values}'"
    end
  end

  failure_message do
    failures = failed_ids

    message = "expected cached code value at ids '"
    message += failures.map(&:id).join("', '")
    message += "' to eq '"
    message += failures.map(&:expected).join("', '")
    message += "', but were '"
    message += failures.map(&:actual).join("', '")
    message + "'."
  end

  failure_message_when_negated do
    failures = failed_ids

    message = "expected cached code value at ids '"
    message += failures.map(&:id).join("', '")
    message += "' not to eq '"
    message += failures.map(&:expected).join("', '")
    message + "'."
  end
end
RSpec::Matchers.alias_matcher :cache_code_value, :cache_code_values

RSpec::Matchers.define :cache_records do |expected_records|
  match do
    expected_records = Array(expected_records)
    actual_records.each_with_index do |actual_record, index|
      expect(actual_record).to eq(expected_records[index])
    end
  end

  define_method :actual_code_values do
    Array(defined?(code_values) && code_values || code_value)
  end

  define_method :actual_records do
    actual_code_values.map do |code_value|
      key = "immutable/immutable_records/code_values/#{code_value}"
      Rails.cache.read(key)
    end
  end

  define_method :failed_code_values do
    failures = []

    actual_records.each_with_index do |actual_record, index|
      code_value = actual_code_values[index]
      expected_record = Array(expected_records)[index]

      next if actual_record == expected_record

      failures << {
        code_value: code_value,
        actual: actual_record,
        expected: expected_record
      }
    end

    failures
  end

  description do
    records = Array(expected_records)

    if records.one?
      "cache record '#{records.first}'"
    else
      "cache records '#{records}'"
    end
  end

  failure_message do
    failures = failed_code_values

    message = "expected cached record at code values '"
    message += failures.map(&:code_value).join("', '")
    message += "' to eq '"
    message += failures.map(&:expected).join("', '")
    message += "', but were '"
    message += failures.map(&:actual).join("', '")
    message + "'."
  end

  failure_message_when_negated do
    failures = failed_code_values

    message = "expected cached records at code values '"
    message += failures.map(&:code_value).join("', '")
    message += "' not to eq '"
    message += failures.map(&:expected).join("', '")
    message + "'."
  end
end
RSpec::Matchers.alias_matcher :cache_record, :cache_records
