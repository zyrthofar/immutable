# frozen_string_literal: true

require 'action_controller/railtie'

module Immutable
  class Application < Rails::Application
    config.eager_load = false

    config.action_controller.perform_caching = true
    config.cache_store = :memory_store
  end
end
