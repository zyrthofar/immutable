# frozen_string_literal: true

ActiveRecord::Schema.define do
  self.verbose = false

  create_table :immutable_records, force: true do |t|
    t.string :code
  end

  create_table :integer_code_immutable_records, force: true do |t|
    t.integer :code
  end

  create_table :associated_records, force: true do |t|
    t.integer :immutable_record_id
    t.integer :immutable_polymorphic_record_id
    t.string :immutable_polymorphic_record_type
  end

  create_table :associated_record_immutable_records, force: true do |t|
    t.integer :associated_record_id
    t.integer :immutable_record_id
  end
end
