# frozen_string_literal: true

RSpec.describe ActiveRecord::Associations::HasManyImmutableAssociation do
  before(:example) { ImmutableRecord.clear_cache! }

  let!(:instance) do
    create(:associated_record,
           immutable_records: immutable_records)
      .tap(&:reload)
  end

  let(:immutable_records) do
    [
      create(:immutable_record, code: 'CODE1'),
      create(:immutable_record, code: 'CODE2')
    ]
  end

  let(:ids) { immutable_records.map(&:id) }
  let(:code_values) { immutable_records.map(&:code) }

  describe '#immutable_records' do
    subject { instance.immutable_records }

    context 'when the code values and the records are not cached' do
      it { is_expected.to eq(immutable_records) }

      it_behaves_like :code_values_retrieved_from_database do
        let(:expected_ids) { ids }
      end

      # Records were retrieved from the database during code values retrieval.
      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_values }
      end
    end

    context 'when the code value is not cached but the record is cached' do
      before(:example) do
        immutable_records.each do |immutable_record|
          key = 'immutable/immutable_records/code_values/' \
                  "#{immutable_record.code_value}"
          Rails.cache.write(key, immutable_record)
        end
      end

      it { is_expected.to eq(immutable_records) }

      it_behaves_like :code_values_retrieved_from_database do
        let(:expected_ids) { ids }
      end

      # Records in cache were overwritten by those retrieved from the database
      # during code values retrieval.
      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_values }
      end
    end

    context 'when the code value is cached but the record is not cached' do
      before(:example) do
        immutable_records.each do |immutable_record|
          key = "immutable/immutable_records/ids/#{immutable_record.id}"
          Rails.cache.write(key, immutable_record.code_value)
        end
      end

      it { is_expected.to eq(immutable_records) }

      it_behaves_like :code_values_retrieved_from_cache do
        let(:expected_ids) { ids }
      end

      it_behaves_like :records_retrieved_from_database do
        let(:expected_code_values) { code_values }
      end
    end

    context 'when the code value and the record are cached' do
      before(:example) do
        code_values.each do |code_value|
          ImmutableRecord[code_value]
        end
      end

      it { is_expected.to eq(immutable_records) }

      it_behaves_like :code_values_retrieved_from_cache do
        let(:expected_ids) { ids }
      end

      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_values }
      end
    end

    context 'when the immutable records are empty' do
      let(:immutable_records) { [] }
      it { is_expected.to be_empty }
    end
  end

  describe '#immutable_records=' do
    before(:example) do
      create(:immutable_record, code: 'CODE3')
      create(:immutable_record, code: 'CODE4')
    end

    let(:values) { [ImmutableRecord['CODE3'], ImmutableRecord['CODE4']] }

    subject { instance.immutable_records = values }

    it 'correctly updates the associations' do
      subject
      instance.save!

      instance.reload
      expect(instance.immutable_records).to(
        eq([ImmutableRecord['CODE3'], ImmutableRecord['CODE4']])
      )
    end
  end
end
