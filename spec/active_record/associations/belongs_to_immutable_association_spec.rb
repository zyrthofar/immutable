# frozen_string_literal: true

RSpec.describe ActiveRecord::Associations::BelongsToImmutableAssociation do
  before(:example) { ImmutableRecord.clear_cache! }

  let!(:instance) do
    create(:associated_record,
           immutable_record: immutable_record)
      .tap(&:reload)
  end

  let(:immutable_record) { create(:immutable_record, code: 'CODE') }
  let(:id) { immutable_record.id }
  let(:code_value) { 'CODE' }

  describe '#immutable_record' do
    subject { instance.immutable_record }

    context 'when the code value and the record are not cached' do
      it { is_expected.to eq(immutable_record) }

      it_behaves_like :code_values_retrieved_from_database do
        let(:expected_ids) { id }
      end

      # Records were retrieved from the database during code values retrieval.
      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_value }
      end
    end

    context 'when the code value is not cached but the record is cached' do
      before(:example) do
        key = "immutable/immutable_records/code_values/#{code_value}"
        Rails.cache.write(key, immutable_record)
      end

      it { is_expected.to eq(immutable_record) }

      it_behaves_like :code_values_retrieved_from_database do
        let(:expected_ids) { id }
      end

      # Records in cache were overwritten by those retrieved from the database
      # during code values retrieval.
      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_value }
      end
    end

    context 'when the code value is cached but the record is not cached' do
      before(:example) do
        key = "immutable/immutable_records/ids/#{id}"
        Rails.cache.write(key, code_value)
      end

      it { is_expected.to eq(immutable_record) }

      it_behaves_like :code_values_retrieved_from_cache do
        let(:expected_ids) { id }
      end

      it_behaves_like :records_retrieved_from_database do
        let(:expected_code_values) { code_value }
      end
    end

    context 'when the code value and the record are cached' do
      before(:example) { ImmutableRecord[code_value] }

      it { is_expected.to eq(immutable_record) }

      it_behaves_like :code_values_retrieved_from_cache do
        let(:expected_ids) { id }
      end

      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_value }
      end
    end

    context 'when the immutable record is nil' do
      let(:immutable_record) { nil }
      it { is_expected.to be_nil }
    end

    context 'when the immutable record is polymorphic' do
      subject { instance.immutable_polymorphic_record }

      it 'raises a NotImplementedError' do
        expect { subject }.to raise_error NotImplementedError
      end
    end
  end

  describe '#immutable_record=' do
    before(:example) { create(:immutable_record, code: 'CODE2') }

    let(:value) { ImmutableRecord['CODE2'] }

    subject { instance.immutable_record = value }

    it 'correctly updates the association' do
      subject
      instance.save!

      instance.reload
      expect(instance.immutable_record).to eq(ImmutableRecord['CODE2'])
    end
  end

  describe '#immutable_record construction' do
    %i[create create! build].each do |method|
      describe "with #{method}" do
        subject { instance.immutable_record.send(method) }

        it 'does not construct a record' do
          expect { subject }.to raise_error(NoMethodError)
        end
      end
    end
  end
end
