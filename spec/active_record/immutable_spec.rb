# frozen_string_literal: true

RSpec.describe ActiveRecord::Immutable do
  before(:example) { ImmutableRecord.clear_cache! }

  let!(:instance) do
    create(:immutable_record, code: 'CODE')
  end

  let(:id) { instance.id }
  let(:code_value) { 'CODE' }

  let(:integer_code_instance) do
    create(:integer_code_immutable_record, code: 100)
  end

  describe '#code_value' do
    subject { instance.code_value }
    it { is_expected.to eq(code_value) }
  end

  describe '.[]' do
    subject { ImmutableRecord[code_value] }

    context 'when accessing with an active record' do
      context 'of the same model' do
        let(:code_value) { instance }
        it { is_expected.to eq(code_value) }
      end

      context 'of a different model' do
        let(:code_value) { create(:associated_record) }
        it { is_expected.to be_nil }
      end
    end

    context 'when accessing with an integer' do
      context 'and the code is an integer' do
        before(:example) { integer_code_instance }

        subject { IntegerCodeImmutableRecord[code_value] }

        context 'and the value is a code in the database' do
          let(:code_value) { 100 }
          it { is_expected.to eq(integer_code_instance) }
        end

        context 'but the value is not a code in the database' do
          let(:code_value) { -1 }
          it { is_expected.to be_nil }
        end
      end

      context 'and the code is a string' do
        before(:example) { instance.update!(code: '100') }

        context 'and the value is a code in the database' do
          let(:code_value) { 100 }
          it { is_expected.to eq(instance) }
        end

        context 'but the value is not a code in the database' do
          let(:code_value) { -1 }
          it { is_expected.to be_nil }
        end
      end
    end

    context 'when accessing with a string' do
      context 'and the value is a code in the database' do
        let(:code_value) { 'CODE' }
        it { is_expected.to eq(instance) }
      end

      context 'but the value is different in case' do
        let(:code_value) { 'code' }
        it { is_expected.to be_nil }
      end

      context 'but the value is not a code in the database' do
        let(:code_value) { 'invalid' }
        it { is_expected.to be_nil }
      end
    end

    context 'when accessing with a symbol' do
      context 'and the value is a code in the database' do
        let(:code_value) { :CODE }
        it { is_expected.to eq(instance) }
      end

      context 'but the value is different in case' do
        let(:code_value) { :code }
        it { is_expected.to be_nil }
      end

      context 'but the value is not a code in the database' do
        let(:code_value) { :invalid }
        it { is_expected.to be_nil }
      end
    end

    context 'when accessing with anything else' do
      let(:code_value) { nil }
      it { is_expected.to be_nil }
    end
  end

  describe '.code_value_cached?' do
    subject { ImmutableRecord.code_value_cached?(id) }

    context 'when the code value is not yet cached' do
      it { is_expected.to eq(false) }
    end

    context 'when the code value is already cached' do
      before(:example) { ImmutableRecord.retrieve_code_value(id) }
      it { is_expected.to eq(true) }

      context 'but it is nil' do
        let(:id) { -1 }
        it { is_expected.to eq(true) }
      end
    end
  end

  describe '.record_cached?' do
    subject { ImmutableRecord.record_cached?(code_value) }

    context 'when the record is not yet cached' do
      it { is_expected.to eq(false) }
    end

    context 'when the record is already cached' do
      before(:example) { ImmutableRecord.retrieve_record(code_value) }
      it { is_expected.to eq(true) }

      context 'but it is nil' do
        let(:code_value) { 'unknown_code_value' }
        it { is_expected.to eq(true) }
      end
    end
  end

  describe '.retrieve_code_value' do
    subject { ImmutableRecord.retrieve_code_value(id) }

    context 'when the code value is not yet cached' do
      it { is_expected.to eq(code_value) }
      it { is_expected.to cache_code_value(code_value) }
      it { is_expected.to cache_record(instance) }

      it_behaves_like :code_values_retrieved_from_database do
        let(:expected_ids) { id }
      end

      context 'but the id is not found in the database' do
        let(:id) { -1 }
        it { is_expected.to be_nil }
        it { is_expected.to cache_code_value(nil) }

        it_behaves_like :code_values_retrieved_from_database do
          let(:expected_ids) { id }
        end
      end
    end

    context 'when the code value is already cached' do
      before(:example) { ImmutableRecord.retrieve_code_value(id) }

      it { is_expected.to eq(code_value) }

      it_behaves_like :code_values_retrieved_from_cache do
        let(:expected_ids) { id }
      end

      context 'but it is nil' do
        let(:id) { -1 }

        it { is_expected.to be_nil }

        it_behaves_like :code_values_retrieved_from_cache do
          let(:expected_ids) { id }
        end
      end
    end
  end

  describe '.retrieve_code_values' do
    let!(:instances) { [instance, create(:immutable_record, code: 'CODE2')] }
    let(:ids) { instances.map(&:id) }
    let(:code_values) { %w[CODE CODE2] }

    subject { ImmutableRecord.retrieve_code_values(ids) }

    context 'when the code values are not yet cached' do
      it { is_expected.to eq(code_values) }
      it { is_expected.to cache_code_values(code_values) }
      it { is_expected.to cache_records(instances) }

      it_behaves_like :code_values_retrieved_from_database do
        let(:expected_ids) { ids }
      end

      context 'but the ids are not found in the database' do
        let(:ids) { [-1, -2] }
        it { is_expected.to be_empty }
        it { is_expected.to cache_code_values(nil) }

        it_behaves_like :code_values_retrieved_from_database do
          let(:expected_ids) { ids }
        end
      end
    end

    context 'when some code values are cached' do
      before(:example) { ImmutableRecord.retrieve_code_value(ids.first) }

      it { is_expected.to eq(code_values) }

      it_behaves_like :code_values_retrieved_from_cache do
        let(:expected_ids) { ids.first }
      end

      it_behaves_like :code_values_retrieved_from_database do
        let(:expected_ids) { ids.second }
      end
    end

    context 'when the code values are already cached' do
      before(:example) { ImmutableRecord.retrieve_code_values(ids) }

      it { is_expected.to eq(code_values) }

      it_behaves_like :code_values_retrieved_from_cache do
        let(:expected_ids) { ids }
      end

      context 'but they are nil' do
        let(:ids) { [-1, -2] }

        it { is_expected.to be_empty }

        it_behaves_like :code_values_retrieved_from_cache do
          let(:expected_ids) { ids }
        end
      end
    end
  end

  describe '.retrieve_record' do
    subject { ImmutableRecord.retrieve_record(code_value) }

    context 'when the record is not yet cached' do
      it { is_expected.to eq(instance) }
      it { is_expected.to cache_code_value(code_value) }
      it { is_expected.to cache_record(instance) }

      it_behaves_like :records_retrieved_from_database do
        let(:expected_code_values) { code_value }
      end

      context 'and the specified code value is a symbol' do
        let(:code_value) { :CODE }
        it { is_expected.to eq(instance) }
        it { is_expected.to cache_code_value(code_value) }
        it { is_expected.to cache_record(instance) }
      end

      context 'but the code value is not found in the database' do
        let(:code_value) { 'unknown_code_value' }

        it { is_expected.to be_nil }
        it { is_expected.to cache_code_value(nil) }
        it { is_expected.to cache_record(nil) }

        it_behaves_like :records_retrieved_from_database do
          let(:expected_code_values) { code_value }
        end
      end
    end

    context 'when the code value is already cached' do
      before(:example) { ImmutableRecord.retrieve_record(code_value) }

      it { is_expected.to eq(instance) }

      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_value }
      end

      context 'but it is nil' do
        let(:code_value) { 'unknown_code_value' }

        it { is_expected.to be_nil }

        it_behaves_like :records_retrieved_from_cache do
          let(:expected_code_values) { code_value }
        end
      end

      # TODO: Test expiration. Rails.cache seems to use the system clock (?).
      # Add `include ActiveSupport::Testing::TimeHelpers` inside the main
      # description.
      #
      # context 'and the value is about to expire' do
      #   around(:example) do |example|
      #     travel_to(1.week.from_now - 1.second) { example.run }
      #   end
      #
      #   it_behaves_like :record_retrieved_from_cache do
      #     let(:expected_code_values) { code_value }
      #   end
      # end
      #
      # context 'but the value is expired' do
      #   around(:example) do |example|
      #     travel_to(1.week.from_now) { example.run }
      #   end
      #
      #   it_behaves_like :record_retrieved_from_cache do
      #     let(:expected_code_values) { code_value }
      #   end
      # end
    end
  end

  describe '.retrieve_records' do
    let!(:instances) { [instance, create(:immutable_record, code: 'CODE2')] }
    let(:code_values) { %w[CODE CODE2] }

    subject { ImmutableRecord.retrieve_records(code_values) }

    context 'when the records are not yet cached' do
      it { is_expected.to eq(instances) }
      it { is_expected.to cache_code_values(code_values) }
      it { is_expected.to cache_records(instances) }

      it_behaves_like :records_retrieved_from_database do
        let(:expected_code_values) { code_values }
      end

      context 'but the ids are not found in the database' do
        let(:code_values) { %w[unknown_1 unknown_2] }
        it { is_expected.to be_empty }
        it { is_expected.to cache_code_values(nil) }

        it_behaves_like :records_retrieved_from_database do
          let(:expected_code_values) { code_values }
        end
      end
    end

    context 'when some records are cached' do
      before(:example) { ImmutableRecord.retrieve_record(code_values.first) }

      it { is_expected.to eq(instances) }

      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_values.first }
      end

      it_behaves_like :records_retrieved_from_database do
        let(:expected_code_values) { code_values.second }
      end
    end

    context 'when the records are already cached' do
      before(:example) { ImmutableRecord.retrieve_records(code_values) }

      it { is_expected.to eq(instances) }

      it_behaves_like :records_retrieved_from_cache do
        let(:expected_code_values) { code_values }
      end

      context 'but they are nil' do
        let(:code_values) { %w[unknown_1 unknown_2] }

        it { is_expected.to be_empty }

        it_behaves_like :records_retrieved_from_cache do
          let(:expected_code_values) { code_values }
        end
      end
    end
  end

  describe '.clear_cache!' do
    before(:example) { ImmutableRecord[code_value] }

    subject { ImmutableRecord.clear_cache! }

    it 'removes all relevant keys from the cache' do
      expect(ImmutableRecord.code_value_cached?(id)).to be_truthy
      expect(ImmutableRecord.record_cached?(code_value)).to be_truthy

      subject

      expect(ImmutableRecord.code_value_cached?(id)).to be_falsey
      expect(ImmutableRecord.record_cached?(code_value)).to be_falsey
    end
  end
end
